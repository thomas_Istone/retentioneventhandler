﻿using MFiles.VAF;
using MFiles.VAF.Common;
using MFilesAPI;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RetentionEventHandler
{


    /// <summary>
    /// Simple vault application to demonstrate VAF.
    /// </summary>
    public class VaultApplication : VaultApplicationBase
    {
        /// <summary>
        /// Simple configuration member. MFConfiguration-attribute will cause this member to be loaded from the named value storage from the given namespace and key.
        /// Here we override the default alias of the Configuration class with a default of the config member.
        /// The default will be written in the named value storage, if the value is missing.
        /// Use Named Value Manager to change the configurations in the named value storage.
        /// </summary>
        [MFConfiguration("RetentionEventHandler", "config")]
        private Configuration config = new Configuration();


        private PropertyValue CreateLookupPropValue(int propDef, int item)
        {
            Lookup lookup = new Lookup();
            lookup.Item = item;
            PropertyValue propValue = new PropertyValueClass();
            propValue.PropertyDef = propDef;
            TypedValue value = new TypedValueClass();
            value.SetValueToLookup(lookup);

            propValue.TypedValue = value;
            return propValue;
        }
        protected override void StartApplication()
        {
            this.BackgroundOperations.RunOnce("Retention Rule Application", () =>

            //this.BackgroundOperations.StartRecurringBackgroundOperation("Retention Rule Application", TimeSpan.FromHours(6), () =>
            {
                //Only for debug
                Debugger.Launch();

                List<ObjVerEx> retentionRules = GetAllActiveRetentionRules(this.PermanentVault);
                foreach (var retentionRule in retentionRules)
                {
                    RetentionRule rule = new RetentionRule(config, this.PermanentVault, retentionRule);
                    ObjVerEx ruleAfterUpdates = rule.ApplyRule();
                    ruleAfterUpdates.SaveProperties();
                    //var rule = retentionRule;

                    //ApplyRetentionRule(this.PermanentVault, ref rule);
                }
            });
        }

        private void ApplyRetentionRule(Vault vault, ref ObjVerEx rule)
        {
            var documents = FindRelatedDocuments(vault, rule);
            var retentionActionId = rule.Properties.GetProperty(config.retentionRuleActionProperty).TypedValue.GetLookupID();
            foreach (var document in documents)
            {
                try
                {
                    switch (retentionActionId)
                    {
                        case 1:
                            document.CheckOut();
                            TypedValue tv = new TypedValue();
                            tv.SetValue(MFDataType.MFDatatypeBoolean, true);
                            document.Properties.Add(new PropertyValue()
                            {
                                PropertyDef = vault.PropertyDefOperations.GetBuiltInPropertyDef(MFBuiltInPropertyDef.MFBuiltInPropertyDefMarkedForArchiving).ID,
                                TypedValue = tv
                            });
                            document.CheckIn();
                            break;
                        case 2:
                            document.Delete();
                            break;
                        case 3:
                            document.Destroy();
                            break;
                        default:
                            break;
                    }

                }
                catch (Exception ex)
                {
                    SysUtils.ReportErrorToEventLog(ex);
                    //TODO: Set State on Rule

                }

            }
            UpdateRunDates();
            //UpdateLastRunDate
            PropertyValue lastRundateProperty;
            rule.TryGetProperty(config.LastRundateProperty, out lastRundateProperty);
            if (lastRundateProperty == null)
            {
                lastRundateProperty = new PropertyValueClass();
                lastRundateProperty.PropertyDef = config.LastRundateProperty;
            }
            TypedValue lastRunDate = new TypedValue();
            lastRunDate.SetValue(MFDataType.MFDatatypeDate, DateTime.Today);
            lastRundateProperty.TypedValue = lastRunDate;
            rule.Properties.SetProperty(lastRundateProperty);

            //UpdateNextRunDate
            var interval = rule.GetProperty(config.IntervalProperty).TypedValue.Value;


            rule.Properties.SetProperty(config.NextRunTimeProperty, MFDataType.MFDatatypeDate, DateTime.Today.AddDays((int)rule.Properties.GetProperty(config.IntervalProperty).TypedValue.Value));
            rule.SaveProperties();
        }

        private void UpdateRunDates()
        {
            throw new NotImplementedException();
        }

        private List<ObjVerEx> FindRelatedDocuments(Vault vault, ObjVerEx rule)
        {
            int retentionPeriod = int.Parse(rule.Properties.GetProperty(config.RetentionPeriodProperty).TypedValue.DisplayValue);
            PropertyDef dateProperty = null;
            int dateCriteria = rule.Properties.GetProperty(config.RetentionCriteriaProperty).TypedValue.GetLookupID();
            switch (dateCriteria)
            {
                case 1:
                    dateProperty = vault.PropertyDefOperations.GetBuiltInPropertyDef(MFBuiltInPropertyDef.MFBuiltInPropertyDefCreated);
                    break;
                case 2:
                    dateProperty = vault.PropertyDefOperations.GetBuiltInPropertyDef(MFBuiltInPropertyDef.MFBuiltInPropertyDefLastModified);
                    break;
                case 3:
                    string manualPropAlias = rule.Properties.GetProperty(config.manualAliasProperty).TypedValue.DisplayValue;
                    dateProperty = vault.PropertyDefOperations.GetPropertyDef(vault.PropertyDefOperations.GetPropertyDefIDByAlias(manualPropAlias));
                    break;
                default:
                    throw new Exception("dateCriteria missing!");

            }

            MFSearchBuilder documentsSearch = new MFSearchBuilder(vault);

            SearchCondition dateCondition = new SearchConditionClass();
            dateCondition.ConditionType = MFConditionType.MFConditionTypeLessThan;
            dateCondition.Expression.DataPropertyValuePropertyDef = dateProperty.ID;
            DateTime searchDate = DateTime.Today.AddMonths(retentionPeriod * -1);
            dateCondition.TypedValue.SetValue(dateProperty.DataType, searchDate);
            documentsSearch.Conditions.Add(-1, dateCondition);

            documentsSearch.Deleted(false);
            documentsSearch.Class((int)rule.Properties.GetProperty(config.DocumentClassProperty).TypedValue.GetLookupID());
            documentsSearch.StatusNot(MFStatusType.MFStatusTypeCheckedOut, MFDataType.MFDatatypeBoolean, true);
            var documents = documentsSearch.FindEx();
            return documents;
        }

        private List<ObjVerEx> GetAllActiveRetentionRules(Vault vault)
        {
            List<ObjVerEx> rulesToRun = new List<ObjVerEx>();
            MFSearchBuilder searchActiveRetentionRules = new MFSearchBuilder(vault);
            searchActiveRetentionRules.Deleted(false);
            searchActiveRetentionRules.ObjType(config.RetentionRuleObjectType);
            searchActiveRetentionRules.Property(config.ActiveProperty, MFDataType.MFDatatypeBoolean, true);
            foreach (var rule in searchActiveRetentionRules.FindEx())
            {
                var nextRunDate = DateTime.Parse(rule.GetProperty(config.NextRunTimeProperty).TypedValue.Value.ToString());
                if (nextRunDate <= DateTime.Today)
                {
                    rulesToRun.Add(rule);
                }
            }
            return rulesToRun;


        }


        [EventHandler(MFEventHandlerType.MFEventHandlerBeforeCreateNewObjectFinalize)]

        //[EventHandler(MFEventHandlerType.MFEventHandlerAfterCreateNewObjectFinalize)]
        public void CreateRetentionRule(EventHandlerEnvironment env)
        {
            if (env.ObjVer.Type == config.RetentionRuleObjectType)
            {
                var rule = env.ObjVerEx;
                TypedValue value = new TypedValue();
                //value.SetValue(MFDataType.MFDatatypeDate, DateTime.Today.AddDays((int)rule.Properties.GetProperty(config.IntervalProperty).TypedValue.Value));
                value.SetValue(MFDataType.MFDatatypeDate, rule.GetProperty(config.StartDateProperty).TypedValue.Value);
                PropertyValue pv = new PropertyValueClass()
                {
                    PropertyDef = config.NextRunTimeProperty,
                    TypedValue = value
                };

                env.Vault.ObjectPropertyOperations.SetProperty(env.ObjVer, pv);

            }
        }

        private void CreatePeriodicTask(EventHandlerEnvironment env)
        {
            PropertyValues periodicTaskPropertryValues = new PropertyValuesClass();
            try
            {
                periodicTaskPropertryValues.Add(CreateLookupPropValue(100, config.RetetionPeridodicTaskClass));
                periodicTaskPropertryValues.Add(CreateLookupPropValue(config.TimeUnitProperty, 2));
                periodicTaskPropertryValues.AddLookup(config.RetentionRuleProperty, env.ObjVer.ID);
                periodicTaskPropertryValues.Add(CreateLookupPropValue(38, config.PeriodicRetentionWorkFlow));
                if ((bool)env.ObjVerEx.Properties.GetProperty(config.ActiveProperty).TypedValue.Value)
                {
                    periodicTaskPropertryValues.Add(CreateLookupPropValue(39, config.ManagementScheduledState));
                }
                else
                {
                    periodicTaskPropertryValues.Add(CreateLookupPropValue(39, config.ManagementOnHoldState));
                }
                var debugTittleProperties = env.ObjVerEx.Properties;
                var debugTitle = debugTittleProperties.GetProperty(config.RuleTitleProperty);

                periodicTaskPropertryValues.Add(new PropertyValueClass
                {
                    PropertyDef = config.TaskTitleProperty,
                    TypedValue = new TypedValueClass
                    {
                        Value = "Periodic Task" + env.ObjVerEx.Properties.GetProperty(config.RuleTitleProperty).TypedValue.Value.ToString()
                    }
                });

                periodicTaskPropertryValues.Add(new PropertyValueClass
                {
                    PropertyDef = config.IntervalProperty,
                    TypedValue = new TypedValueClass
                    {
                        Value = 1
                    }
                });

                TypedValue typedValue = new TypedValueClass();
                typedValue.SetValue(MFDataType.MFDatatypeDate, env.ObjVerEx.Properties.GetProperty(config.StartDateProperty).TypedValue.Value);
                periodicTaskPropertryValues.Add(new PropertyValueClass
                {
                    PropertyDef = config.NextRunTimeProperty,
                    TypedValue = typedValue
                });
            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog(ex);
                throw ex;
            }
            try
            {
                var retentionPeriodicTask = env.Vault.ObjectOperations.CreateNewObject(config.RetentionPeriodicTaskObjectType, periodicTaskPropertryValues);
                env.Vault.ObjectOperations.CheckIn(retentionPeriodicTask.ObjVer);
            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog(ex);
                throw ex;
            }
        }
    }
}