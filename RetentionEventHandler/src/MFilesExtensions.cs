﻿using MFilesAPI;

namespace RetentionEventHandler
{
    public static class MFilesExtensions
    {
        public static void Add(this PropertyValues propValues, PropertyValue propValue)
        {
            propValues.Add(propValues.Count, propValue);
        }

    }
}
