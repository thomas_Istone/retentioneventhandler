﻿using MFiles.VAF.Common;

namespace RetentionEventHandler
{
    /// <summary>
    /// Simple configuration.
    /// </summary>
    public class Configuration
    {
        /// <summary>
        /// Reference to a test class.
        /// </summary>

        [MFObjType(Required = true)]
        public MFIdentifier RetentionRuleObjectType = "iStoneRetention.RetentionRule.ObjectType";

        [MFObjType(Required = true)]
        public MFIdentifier RetentionPeriodicTaskObjectType = "iStoneRetention.RetentionPeriodicTask.ObjectType";

        [MFClass(Required = true)]
        public MFIdentifier RetetionPeridodicTaskClass = "iStoneRetention.RetentionPeriodicTask.Class";

        [MFPropertyDef(Required = true)]
        public MFIdentifier StartDateProperty = "iStoneRetention.RetentionRuleStartDate.Property";
        [MFPropertyDef(Required = true)]
        public MFIdentifier ActiveProperty = "iStoneRetention.RetentionRuleActive.Property";

        [MFPropertyDef(Required = true)]
        public MFIdentifier RuleTitleProperty = "iStoneRetention.RetentionRuleRetentionRuleTitle.Property";

        [MFPropertyDef(Required = true)]
        public MFIdentifier TaskTitleProperty = "iStoneRetention.RetentionPeriodicTaskTitle.Property";
        [MFPropertyDef(Required = true)]
        public MFIdentifier LastRundateProperty = "iStoneRetention.RetentionPeriodicTaskLastRunDate.Property";
        [MFPropertyDef(Required = true)]
        public MFIdentifier IntervalProperty = "iStoneRetention.RetentionPeriodicTaskInterval.Property";

        [MFPropertyDef(Required = true)]
        public MFIdentifier TimeUnitProperty = "iStoneRetention.RetentionPeriodicTaskTimeUnit.Property";

        [MFPropertyDef(Required = true)]
        public MFIdentifier DocumentClassProperty = "iStoneRetention.RetentionRuleDocumentClass.Property";

        [MFPropertyDef(Required = true)]
        public MFIdentifier NextRunTimeProperty = "iStoneRetention.RetentionPeriodicTaskNextRunDate.Property";

        [MFPropertyDef(Required = true)]
        public MFIdentifier RetentionCriteriaProperty = "iStoneRetention.RetentionRuleDateRule.Property";

        [MFPropertyDef(Required = true)]
        public MFIdentifier RetentionPeriodProperty = "iStoneRetention.RetentionRuleRetentionTime.Property";
        [MFPropertyDef(Required = true)]
        public MFIdentifier manualAliasProperty = "iStoneRetention.RetentionRuleManualAlias.Property";
        [MFPropertyDef(Required = true)]
        public MFIdentifier retentionRuleActionProperty = "iStoneRetention.RetentionRuleAction.Property";

        [MFPropertyDef(Required = true)]
        public MFIdentifier RetentionRuleProperty = "iStoneRetention.RetentionPeriodicTaskRetentionRule.Property";

        [MFWorkflow(Required = true)]
        public MFIdentifier PeriodicRetentionWorkFlow = "iStoneRetention.PeriodicRetentionManagement.Workflow";
        [MFPropertyDef(Required = true)]
        public MFIdentifier FailedDocumentsProperty = "iStoneRetention.RetentionRuleFailedDocuments.Property";


        [MFState(Required = true)]
        public MFIdentifier ManagementScheduledState = "iStoneRetention.PeriodicRetentionManagementScheduled.State";

        [MFState(Required = true)]
        public MFIdentifier ManagementOnHoldState = "iStoneRetention.PeriodicRetentionManagementScheduled.State";


    }
}
