﻿using MFiles.VAF.Common;
using MFilesAPI;
using System;
using System.Collections.Generic;
namespace RetentionEventHandler

{
    public class RetentionRule
    {

        private Configuration config;
        public Vault vault { get; set; }
        public ObjVerEx rule { get; set; }
        public List<ObjVerEx> documents { get; set; }
        public RetentionRule(Configuration _config, Vault _vault, ObjVerEx _rule)
        {
            config = _config;
            rule = _rule;
            vault = _vault;
        }

        public ObjVerEx ApplyRule()
        {
            documents = FindRelatedDocuments();
            var retentionActionId = rule.Properties.GetProperty(config.retentionRuleActionProperty).TypedValue.GetLookupID();
            foreach (var document in documents)
            {
                ApplyRuleOnDocument(document, retentionActionId);

            }
            UpdateRunDates();
            return rule;
        }
        public void ApplyRuleOnDocument(ObjVerEx document, int actionId)
        {
            
            try
            {
                switch (actionId)
                {
                    case 1:
                        document.CheckOut();
                        TypedValue tv = new TypedValue();
                        tv.SetValue(MFDataType.MFDatatypeBoolean, true);
                        document.Properties.Add(new PropertyValue()
                        {
                            PropertyDef = vault.PropertyDefOperations.GetBuiltInPropertyDef(MFBuiltInPropertyDef.MFBuiltInPropertyDefMarkedForArchiving).ID,
                            TypedValue = tv
                        });
                        document.CheckIn();
                        break;
                    case 2:
                        document.Delete();
                        break;
                    case 3:
                        document.Destroy();
                        break;

                    default:
                        break;
                }

            }
            catch (Exception ex)
            {
                SysUtils.ReportErrorToEventLog(ex);
                //TODO: Set State on Rule

            }
        }
        private void UpdateRunDates()
        {
            PropertyValue lastRundateProperty;
            rule.TryGetProperty(config.LastRundateProperty, out lastRundateProperty);
            if (lastRundateProperty == null)
            {
                lastRundateProperty = new PropertyValueClass();
                lastRundateProperty.PropertyDef = config.LastRundateProperty;
            }
            TypedValue lastRunDate = new TypedValue();
            lastRunDate.SetValue(MFDataType.MFDatatypeDate, DateTime.Today);
            lastRundateProperty.TypedValue = lastRunDate;
            rule.Properties.SetProperty(lastRundateProperty);

            //UpdateNextRunDate
            var interval = rule.GetProperty(config.IntervalProperty).TypedValue.Value;


            rule.Properties.SetProperty(config.NextRunTimeProperty, MFDataType.MFDatatypeDate, DateTime.Today.AddDays((int)rule.Properties.GetProperty(config.IntervalProperty).TypedValue.Value));
            rule.SaveProperties();
        }
        public List<ObjVerEx> FindRelatedDocuments()
        {
            int retentionPeriod = int.Parse(rule.Properties.GetProperty(config.RetentionPeriodProperty).TypedValue.DisplayValue);
            PropertyDef dateProperty = null;
            int dateCriteria = rule.Properties.GetProperty(config.RetentionCriteriaProperty).TypedValue.GetLookupID();
            switch (dateCriteria)
            {
                case 1:
                    dateProperty = vault.PropertyDefOperations.GetBuiltInPropertyDef(MFBuiltInPropertyDef.MFBuiltInPropertyDefCreated);
                    return SearchBasedOnDateProperty(retentionPeriod, dateProperty);
                case 2:
                    dateProperty = vault.PropertyDefOperations.GetBuiltInPropertyDef(MFBuiltInPropertyDef.MFBuiltInPropertyDefLastModified);
                    return SearchBasedOnDateProperty(retentionPeriod, dateProperty);
                case 3:
                    string manualPropAlias = rule.Properties.GetProperty(config.manualAliasProperty).TypedValue.DisplayValue;
                    dateProperty = vault.PropertyDefOperations.GetPropertyDef(vault.PropertyDefOperations.GetPropertyDefIDByAlias(manualPropAlias));
                    return SearchBasedOnDateProperty(retentionPeriod, dateProperty);
                case 4:
                    return SearchBasedOnAccessDate(retentionPeriod);
                default:
                    throw new Exception("dateCriteria missing!");

            }


        }

        private List<ObjVerEx> SearchBasedOnAccessDate(int retentionPeriod)
        {
            List<ObjVerEx> documentsToDelete = new List<ObjVerEx>();
            MFSearchBuilder documentsSearch = new MFSearchBuilder(vault);
            documentsSearch.Deleted(false);
            documentsSearch.Class((int)rule.Properties.GetProperty(config.DocumentClassProperty).TypedValue.GetLookupID());
            documentsSearch.StatusNot(MFStatusType.MFStatusTypeCheckedOut, MFDataType.MFDatatypeBoolean, true);
            var allDocumentsRelatedToClass = documentsSearch.FindEx();
            foreach (var doc in allDocumentsRelatedToClass)
            {
                var files = vault.ObjectFileOperations.GetFiles(doc.ObjVer);
                foreach (ObjectFile file in files)
                {
                    var lastAccessed = file.LastAccessTimeUtc;
                    if (lastAccessed <= DateTime.Today.AddMonths(retentionPeriod * -1))
                    {
                        documentsToDelete.Add(doc);
                    }
                }
            }
            return documentsToDelete;
        }

        private List<ObjVerEx> SearchBasedOnDateProperty(int retentionPeriod, PropertyDef dateProperty)
        {
            MFSearchBuilder documentsSearch = new MFSearchBuilder(vault);

            SearchCondition dateCondition = new SearchConditionClass();
            dateCondition.ConditionType = MFConditionType.MFConditionTypeLessThan;
            dateCondition.Expression.DataPropertyValuePropertyDef = dateProperty.ID;
            DateTime searchDate = DateTime.Today.AddMonths(retentionPeriod * -1);
            dateCondition.TypedValue.SetValue(dateProperty.DataType, searchDate);
            documentsSearch.Conditions.Add(-1, dateCondition);

            documentsSearch.Deleted(false);
            documentsSearch.Class((int)rule.Properties.GetProperty(config.DocumentClassProperty).TypedValue.GetLookupID());
            documentsSearch.StatusNot(MFStatusType.MFStatusTypeCheckedOut, MFDataType.MFDatatypeBoolean, true);
            var documents = documentsSearch.FindEx();

            return documents;
        }
    }
}
